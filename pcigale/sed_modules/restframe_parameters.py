"""
Module that estimates other parameters, e.g., UV slope, Lick indices, etc.
==========================================================================

This module estimates additional parameters of interest close to the
observation, e.g., the ultraviolet slope (beta), the rest-frame, any type of
indices (Lick), etc.

This module has to be called right before redshifting as such it does not take
into account the IGM absorption so it should not be used to compute fluxes in
the observed frame. For that use the param_postz module.

"""

from itertools import chain

import numpy as np
from numpy._core.multiarray import interp  # Faster compiled interpolation
from scipy.constants import c, parsec

from pcigale.sed.utils import flux_trapz
from pcigale.sed_modules import SedModule

__category__ = "restframe_parameters"


class RestframeParam(SedModule):
    """Compute miscellaneous parameters on the full SED. This is a separate
    module as computing these quantitites in other SED modules does not always
    make much sense. This module is to be called right before the redshifting
    module. This means it does not include IGM absorbtion

    """

    parameters = {
        "beta_calz94": (
            "boolean()",
            "Observed and intrinsic UV slopes β and β₀ measured in the same "
            "way as in Calzetti et al. (1994).",
            False
        ),
        "Dn4000": (
            "boolean()",
            "Dn4000 break using the Balogh et al. (1999) definition.",
            False
        ),
        "IRX": (
            "boolean()",
            "IRX computed from the GALEX FUV filter and the dust luminosity.",
            False
        ),
        "EW": (
            "string()",
            "Equivalent width of emission/absorption lines. This requires a "
            "label for easier recognition in the output file followed by the"
            "the definition of the wavelength ranges of three bands: the "
            "blue sideband, the line, and the red sideband. Each band is "
            "defined by two wavelengths and the wavelengths are separated "
            "by '/'. For instance the HδA is defined as (Worthey et al. "
            "1997): 404.160/407.975/408.350/412.225/412.805/416.100. "
            "Different lines can be provided separated by &. By convention, "
            "absorption lines are positive and emission lines are negative.",
            "HdeltaA/404.160/407.975/408.350/412.225/412.850/416.100 & "
            "Halpha/650.0/652.5/653.5/660.0/661.0/663.5",
        ),
        "luminosity_filters": (
            "string()",
            "Filters for which the rest-frame luminosity will be computed. "
            "You can give several filter names separated by a & (don't use "
            "commas).",
            "galex.FUV & generic.bessell.V"
        ),
        "colours_filters": (
            "string()",
            "Rest-frame colours to be computed. You can give several colours "
            "separated by a & (don't use commas).",
            "galex.FUV-galex.NUV & galex.NUV-sloan.sdss.r"
        )
    }

    @staticmethod
    def _slope(x, y):
        """Computes the slope of y vs x from the covariance matrix.
        """
        X = np.stack((x, y), axis=0)
        X -= np.average(X, axis=1)[:, None]
        ssxm, ssxym, _, _ = np.dot(X, X.T).flat

        return ssxym / ssxm

    def calz94(self, sed):
        wl = sed.wavelength_grid

        # Attenuated (observed) UV slopes beta as defined in Calzetti et al.
        # (1994, ApJ 429, 582, Tab. 2) that excludes the 217.5 nm bump
        # wavelength range and other spectral features
        if 'nebular.lines_width' in sed.info:
            key = (wl.size, sed.info['nebular.lines_width'])
        else:
            key = (wl.size, )

        if key in self.w_calz94:
            w_calz94 = self.w_calz94[key]
        else:
            calz_wl = [(126.8, 128.4), (130.9, 131.6), (134.2, 137.1),
                       (140.7, 151.5), (156.2, 158.3), (167.7, 174.0),
                       (176.0, 183.3), (186.6, 189.0), (193.0, 195.0),
                       (240.0, 258.0)]
            w_calz94 = np.where(np.any([(wl >= wlseg[0]) & (wl <= wlseg[1])
                                        for wlseg in calz_wl], axis=0))

            self.w_calz94[key] = w_calz94

        lgwl = np.log10(wl[w_calz94])
        lglumin = np.log10(sed.luminosity[w_calz94])
        lglumin0 = np.log10(np.sum([sed.luminosities[k][w_calz94]
                                    for k in sed.luminosities
                                    if 'attenuation' not in k], axis=0))

        return (self._slope(lgwl, lglumin), self._slope(lgwl, lglumin0))

    def Dn4000(self, sed):
        # Strength of the Dn4000 break using Balogh et al. (1999, ApJ 527, 54),
        # i.e., ratio of the flux in the red continuum to that in the blue
        # continuum: Blue continuum: 385.0-395.0 nm & red continuum:
        # 400.0-410.0 nm. We use two specially crafted filters so that the
        # ratio of their integral gives Dn4000.

        return sed.compute_fnu("special.dn4000.hi") / sed.compute_fnu("special.Dn4000.lo")

    def EW(self, sed):
        # Equivalent width of emission and absorption lines. Three windows
        # are defined: the red sideband, line, and blue sidebands windows.
        # Each window is described with its minimum and maximum wavelengths.
        # The continuum is computed by interpolating between the two side
        # bands.
        wl = sed.wavelength_grid
        lumin = sed.luminosity

        if "nebular.lines_width" in sed.info:
            key = (wl.size, sed.info["nebular.lines_width"])
        else:
            key = (wl.size,)

        if key in self.ew_cache:
            wl_bands = self.ew_cache[key]
        else:
            wl_bands = {}
            for line, wl_line in self.ew_wl.items():
                w = np.searchsorted(wl, self.ew_wl[line])

                wl_bands[line] = (
                    w,
                    np.hstack((wl_line[0], wl[w[0] : w[1]], wl_line[1])),
                    np.hstack((wl_line[2], wl[w[2] : w[3]], wl_line[3])),
                    np.hstack((wl_line[4], wl[w[4] : w[5]], wl_line[5])),
                )
            self.ew_cache[key] = wl_bands

        EW = {}
        for line, wl_bounds in self.ew_wl.items():
            w, wl_cont1, wl_line, wl_cont2 = wl_bands[line]

            lum_bounds = interp(wl_bounds, wl, lumin)
            lum_cont1 = np.concatenate(
                ((lum_bounds[0],), lumin[w[0] : w[1]], (lum_bounds[1],))
            )
            lum_line = np.concatenate(
                ((lum_bounds[2],), lumin[w[2] : w[3]], (lum_bounds[3],))
            )
            lum_cont2 = np.concatenate(
                ((lum_bounds[4],), lumin[w[4] : w[5]], (lum_bounds[5],))
            )

            mean_wl_cont1 = 0.5 * (wl_cont1[0] + wl_cont1[-1])
            mean_wl_cont2 = 0.5 * (wl_cont2[0] + wl_cont2[-1])

            linekey = key + (line, 0.0)
            mean_lum_cont1 = flux_trapz(
                lum_cont1, wl_cont1, linekey + ("cont1",)
            ) / (wl_cont1[-1] - wl_cont1[0])
            mean_lum_cont2 = flux_trapz(
                lum_cont2, wl_cont2, linekey + ("cont2",)
            ) / (wl_cont2[-1] - wl_cont2[0])

            lum_cont = mean_lum_cont1 + (mean_lum_cont2 - mean_lum_cont1) / (
                mean_wl_cont2 - mean_wl_cont1
            ) * (wl_line - mean_wl_cont1)

            EW[line] = (
                flux_trapz(lum_line - lum_cont, wl_line, linekey)
                / flux_trapz(lum_cont, wl_line, linekey)
                * (wl_line[-1] - wl_line[0])
            )

        return EW

    def _init_code(self):
        # Index of the wavelengths of interest. We use a dictionary with the
        # size of the wavelength array as a key to take into account that
        # different models may have a different sampling, for instance when
        # an AGN may be present or not.
        self.w_calz94 = {}
        self.w_D4000blue = {}
        self.w_D4000red = {}
        self.ew_cache = {}

        # Extract the list of lines to compute the equivalent width
        self.ew_wl = [
            item.strip()
            for item in self.parameters["EW"].split("&")
            if item.strip() != ""
        ]
        self.ew_wl = {
            line.split("/")[0]: tuple(float(wl) for wl in line.split("/")[1:])
            for line in self.ew_wl
        }

        # Extract the list of filters to compute the luminosities
        self.lumin_filters = [item.strip() for item in
                              self.parameters["luminosity_filters"].split("&")
                              if item.strip() != '']

        # Extract the list of rest-frame colours
        self.colours = [item.strip().split("-") for item in
                        self.parameters["colours_filters"].split("&")
                        if item.strip() != '']

        # Extract the list of rest-frame colour filters
        self.colour_filters = list(set(chain(*self.colours)))

        # Compute the list of unique filters to compute both the luminosities
        # and the colours
        self.filters = list(set(self.lumin_filters + self.colour_filters +
                                (["galex.FUV"] if self.parameters["IRX"]
                                 else [])))

        # Conversion factor to go from Fnu in mJy to Lnu in W
        self.to_lumin = 1e-29 * 4. * np.pi * (10. * parsec)**2

    def process(self, sed):
        """Computes the parameters for each model.

        Parameters
        ----------
        sed: pcigale.sed.SED object

        """
        fluxes = {filt: sed.compute_fnu(filt) for filt in self.filters}

        if self.parameters['beta_calz94']:
            beta, beta0 = self.calz94(sed)
            sed.add_info("param.beta_calz94", beta)
            sed.add_info("param.beta0_calz94", beta0)
        if self.parameters['Dn4000']:
            sed.add_info("param.Dn4000", self.Dn4000(sed))

        if self.parameters['IRX']:
            sed.add_info("param.IRX", np.log10(sed.info['dust.luminosity'] /
                         (fluxes['galex.FUV'] * self.to_lumin * c / 154e-9)))

        for line, EW in self.EW(sed).items():
            sed.add_info(f"param.EW({line})", EW, unit="nm")

        for filt in self.lumin_filters:
            sed.add_info(f"param.restframe_Lnu({filt})",
                         fluxes[filt] * self.to_lumin, True, unit='W/Hz')
        for filt1, filt2 in self.colours:
            sed.add_info(f"param.restframe_{filt1}-{filt2}",
                         2.5 * np.log10(fluxes[filt2] / fluxes[filt1]),
                         unit='mag')


# SedModule to be returned by get_module
Module = RestframeParam
