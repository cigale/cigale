import warnings

import numpy as np
from astropy.table import Column, Table
from astropy.utils.exceptions import AstropyUserWarning

warnings.simplefilter('ignore', AstropyUserWarning)


def save_sed_to_fits(sed, prefix, norm=1.0):
    """
    Save a SED object to fits files

    Parameters
    ----------
    sed: a pcigale.sed.SED object
        The SED to save
    prefix: string
        Prefix of the fits file containing the path and the id of the model
    norm: float
        Normalisation factor of the SED

    """
    info = {}
    for name in sed.info:
        # Define a new card name replacing / with 0 and limit to 40 characters
        # This is done to avoid issues with some strict parsers.
        cardname = name.replace("/", "-")[:40]  # / can generate issues

        if name in sed.mass_proportional_info:
            info[cardname] = str(norm * sed.info[name])
        else:
            info[cardname] = str(sed.info[name])

    table = Table(meta=info)
    table['wavelength'] = Column(sed.wavelength_grid, unit="nm")
    table['Fnu'] = Column(norm * sed.fnu, unit="mJy")
    table['L_lambda_total'] = Column(norm * sed.luminosity, unit="W/nm")
    for name in sed.luminosities:
        table[name] = Column(norm * sed.luminosities[name], unit="W/nm")
    table.write(f"{prefix}_best_model.fits")

    if sed.sfh is not None:
        table = Table(meta=info)
        table["time"] = Column(np.arange(sed.sfh.size), unit="Myr")
        table["SFR"] = Column(norm * sed.sfh, unit="Msun/yr")
        table.write(f"{prefix}_SFH.fits")
