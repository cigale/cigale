from setuptools import find_namespace_packages, setup
from setuptools.command.build import build


class CustomBuild(build):
    user_options = [
        ("bc03res=", None, "Resolution of the BC03 models, hr or lr."),
        ("bpassres=", None, "Resolution of the BPASS models, hr or lr."),
        ("bpassresmax=", None, "Maximum resolution of the BPASS models."),
        ("agemax=", None, "Maximum age of the stellar populations."),
    ]
    description = "Build the pcigale database."

    def initialize_options(self):
        super().initialize_options()
        self.bc03res = "lr"
        self.bpassres = "lr"
        self.bpassresmax = None
        self.agemax = 13700

    def finalize_options(self):
        assert self.bc03res in ("lr", "hr"), "bc03res must be hr or lr!"
        assert self.bpassres in ("lr", "hr"), "bpassres must be hr or lr!"
        super().finalize_options()

    def run(self):
        # Build the database.
        import database_builder

        database_builder.build_base(
            self.bc03res, self.bpassres, self.bpassresmax, self.agemax
        )

        # Proceed with the build
        super().run()


entry_points = {
    "console_scripts": [
        "pcigale = pcigale:main",
        "pcigale-plots = pcigale_plots:main",
        "pcigale-filters = pcigale_filters:main",
    ]
}

with open("pcigale/version.py") as f:
    exec(f.read())

setup(
    name="pcigale",
    version=__version__,
    packages=find_namespace_packages(include=["pcigale*"],),
    install_requires=[
        "numpy",
        "scipy",
        "matplotlib",
        "configobj",
        "astropy",
        "rich",
        "importlib_resources",
    ],
    setup_requires=[
        "setuptools",
        "numpy",
        "scipy",
        "matplotlib",
        "configobj",
        "astropy",
        "rich",
        "importlib_resources",
    ],
    entry_points=entry_points,
    cmdclass={"build": CustomBuild},
    package_data={
        "pcigale": ["data/*/*.pickle", "sed_modules/curves/*.dat"],
        "pcigale_plots": ["resources/CIGALE.png"],
    },
    include_package_data=True,
    author="The CIGALE team",
    author_email="cigale@lam.fr",
    url="https://cigale.lam.fr",
    description="Python Code Investigating Galaxy Emission",
    long_description=open("README.rst").read(),
    long_description_content_type="text/x-rst",
    license="CECILL-2.0",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: CeCILL-2.0 License",
        "Operating System :: OS Independent",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Astronomy",
    ],
    keywords="astrophysics, galaxy, SED fitting",
)
