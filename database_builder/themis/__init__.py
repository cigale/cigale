"""This script is used the build pcigale internal database containing."""

import io
from pathlib import Path

import numpy as np
import scipy.constants as cst

from pcigale.data import SimpleDatabase
from pcigale.utils.console import console
from pcigale.utils.counter import Counter


def build():
    console.rule("THEMIS dust models")

    path = Path(__file__).parent / "data"
    db = SimpleDatabase("themis", writable=True)

    # Mass fraction of hydrocarbon solids i.e., a-C(:H) smaller than 1.5 nm,
    # also known as HAC
    qhac = {
        "000": 0.02,
        "010": 0.06,
        "020": 0.10,
        "030": 0.14,
        "040": 0.17,
        "050": 0.20,
        "060": 0.24,
        "070": 0.28,
        "080": 0.32,
        "090": 0.36,
        "100": 0.40,
    }

    uminimum = [
        "0.100",
        "0.120",
        "0.150",
        "0.170",
        "0.200",
        "0.250",
        "0.300",
        "0.350",
        "0.400",
        "0.500",
        "0.600",
        "0.700",
        "0.800",
        "1.000",
        "1.200",
        "1.500",
        "1.700",
        "2.000",
        "2.500",
        "3.000",
        "3.500",
        "4.000",
        "5.000",
        "6.000",
        "7.000",
        "8.000",
        "10.00",
        "12.00",
        "15.00",
        "17.00",
        "20.00",
        "25.00",
        "30.00",
        "35.00",
        "40.00",
        "50.00",
        "80.00",
    ]

    alpha = [
        "1.0",
        "1.1",
        "1.2",
        "1.3",
        "1.4",
        "1.5",
        "1.6",
        "1.7",
        "1.8",
        "1.9",
        "2.0",
        "2.1",
        "2.2",
        "2.3",
        "2.4",
        "2.5",
        "2.6",
        "2.7",
        "2.8",
        "2.9",
        "3.0",
    ]

    # Mdust/MH used to retrieve the dust mass as models as given per atom of H
    MdMH = {
        "000": 7.4e-3,
        "010": 7.4e-3,
        "020": 7.4e-3,
        "030": 7.4e-3,
        "040": 7.4e-3,
        "050": 7.4e-3,
        "060": 7.4e-3,
        "070": 7.4e-3,
        "080": 7.4e-3,
        "090": 7.4e-3,
        "100": 7.4e-3,
    }

    # Here we obtain the wavelength beforehand to avoid reading it each time.
    filename = path / "U0.100_0.100_MW3.1_000" / "spec_1.0.dat"
    with filename.open() as datafile:
        data = "".join(datafile.readlines()[-576:])

    wave = np.genfromtxt(io.BytesIO(data.encode()), usecols=(0))

    # We convert wavelengths from μm to nm
    wave *= 1000.0

    # Conversion factor from Jy cm² sr¯¹ H¯¹ to W nm¯¹ (kg of H)¯¹
    conv = 4.0 * np.pi * 1e-30 / (cst.m_p + cst.m_e) * cst.c / (wave * wave) * 1e9

    counter = Counter(len(qhac) * len(uminimum) * (1 + len(alpha)))
    for model in qhac.keys():
        for umin in uminimum:
            filename = path / f"U{umin}_{umin}_MW3.1_{model}" / "spec_1.0.dat"
            with open(filename) as datafile:
                data = "".join(datafile.readlines()[-576:])
            lumin = np.genfromtxt(io.BytesIO(data.encode()), usecols=(2))

            # Conversion from Jy cm² sr¯¹ H¯¹to W nm¯¹ (kg of dust)¯¹
            lumin *= conv / MdMH[model]

            db.add(
                {
                    "qhac": float(qhac[model]),
                    "umin": float(umin),
                    "umax": float(umin),
                    "alpha": 1.0,
                },
                {"wl": wave, "spec": lumin},
            )
            counter.inc()
            for al in alpha:
                filename = path / f"U{umin}_1e7_MW3.1_{model}" / f"spec_{al}.dat"
                with open(filename) as datafile:
                    data = "".join(datafile.readlines()[-576:])
                lumin = np.genfromtxt(io.BytesIO(data.encode()), usecols=(2))

                # Conversion from Jy cm² sr¯¹ H¯¹to W nm¯¹ (kg of dust)¯¹
                lumin *= conv / MdMH[model]

                db.add(
                    {
                        "qhac": float(qhac[model]),
                        "umin": float(umin),
                        "umax": 1e7,
                        "alpha": float(al),
                    },
                    {"wl": wave, "spec": lumin},
                )
                counter.inc()
    db.close()
    counter.progress.join()
