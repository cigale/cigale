"""This script is used the build pcigale internal database containing."""

import itertools
from pathlib import Path

import numpy as np
from scipy import interpolate

from pcigale.data import SimpleDatabase
from pcigale.utils.console import WARNING, console
from pcigale.utils.counter import Counter


def build(bpassres, bpassresmax=None, agemax=13700):
    console.rule("BPASSv2.2 single stellar populations")

    path = Path(__file__).parent / "data"
    db = SimpleDatabase("bpassv2", writable=True)

    # Time grid (1 Myr to 2 Gyr with 1 Myr step)
    agemax = int(agemax)
    ssp_timegrid = np.arange(1, agemax + 1)

    # The wavelength grid of the BPASS models is extremely refined. This is too
    # refined for our purpose. By default we interpolate on the grid of the low
    # resolution BC03 models. We remove the wavelengths beyond 10 microns as
    # they are out of the range covered by BPASS.

    with SimpleDatabase("bc03") as bc03db:
        spec_wave_lr = bc03db.get(imf="salp", Z=0.02).wl
    spec_wave_lr = spec_wave_lr[spec_wave_lr <= 10000.0]

    # Metallicities associated to each key
    metal = {
        "em5": 0.00001,
        "em4": 0.0001,
        "001": 0.001,
        "002": 0.002,
        "003": 0.003,
        "004": 0.004,
        "006": 0.006,
        "008": 0.008,
        "010": 0.010,
        "014": 0.014,
        "020": 0.020,
        "030": 0.030,
        "040": 0.040,
    }

    # IMF associated to each key
    imf = {
        "100_100": 0,
        "100_300": 1,
        "135_100": 2,
        "135_300": 3,
        "135all_100": 4,
        "170_100": 5,
        "170_300": 6,
        "_chab100": 7,
        "_chab300": 8,
    }

    basename = "{}-{}-imf{}.z{}.dat.gz"

    nfiles = len(list(path.rglob("spectra*.dat.gz")))
    counter = Counter(nfiles)
    if nfiles > 0:
        for key_metal, key_imf, binary in itertools.product(metal, imf, ["bin", "sin"]):
            specname = path / basename.format("spectra", binary, key_imf, key_metal)
            ionname = path / basename.format("ionizing", binary, key_imf, key_metal)
            massname = path / basename.format("starmass", binary, key_imf, key_metal)

            # As BPASS models are very large, it is unlikely that they are all
            # available, so we skips the models that are not present
            if not Path(specname).is_file():
                continue

            spec = np.genfromtxt(specname)

            # Get the wavelengths and them from Å to nm
            spec_wave = spec[:, 0] * 0.1

            # Get the ionizing flux and normalize it from 10⁶ Msun to 1 Msun
            ion = 10 ** np.genfromtxt(ionname)[:, 1] * 1e-6

            # Get the stellar mass and normalize it from 10⁶ Msun to 1 Msun
            mass = np.genfromtxt(massname)[:, 1] * 1e-6

            # Convert from Lsun/Å for 10⁶ Msun to W/nm for 1 Msun
            spec = spec[:, 1:] * (10.0 * 3.828e26 * 1e-6)

            if bpassres == "lr":
                spec = 10 ** interpolate.interp1d(
                    np.log10(spec_wave), np.log10(spec), axis=0, assume_sorted=True
                )(np.log10(spec_wave_lr))
                spec[np.where(np.isnan(spec))] = 0.0
                spec_wave = spec_wave_lr
            elif bpassresmax is not None:
                R = float(bpassresmax)
                k = 1.0 + 1.0 / R
                n = np.floor(np.log(10) / np.log(k))
                spec_wave_ir = np.hstack(
                    [spec_wave[: int(R)], (0.1 * R) * k ** np.arange(0, n)]
                )
                spec = 10 ** interpolate.interp1d(
                    np.log10(spec_wave), np.log10(spec), axis=0, assume_sorted=True
                )(np.log10(spec_wave_ir))
                spec[np.where(np.isnan(spec))] = 0.0
                spec_wave = spec_wave_ir

            # Timegrid from the models in Myr
            spec_timegrid = np.logspace(0.0, 5.0, spec.shape[1])

            ssp_lumin = interpolate.interp1d(spec_timegrid, spec)(ssp_timegrid)
            ssp_mass = interpolate.interp1d(spec_timegrid, mass)(ssp_timegrid)
            ssp_ion = interpolate.interp1d(spec_timegrid, ion)(ssp_timegrid)

            ssp_info = np.array([ssp_mass, ssp_ion])

            db.add(
                {
                    "imf": imf[key_imf],
                    "Z": metal[key_metal],
                    "binary": True if "bin" in binary else False,
                },
                {
                    "t": ssp_timegrid,
                    "wl": spec_wave,
                    "info": ssp_info,
                    "spec": ssp_lumin,
                },
            )
            counter.inc()
    else:
        console.print(f"{WARNING} No BPASSv2.2 model found")

    db.close()
    counter.progress.join()
