"""This script is used the build pcigale internal database containing."""

from pathlib import Path

import numpy as np

from pcigale.data import SimpleDatabase
from pcigale.utils.console import console
from pcigale.utils.counter import Counter


def build():
    console.rule("SKIRTOR (2016) AGN models")

    path = Path(__file__).parent / "data"
    db = SimpleDatabase("skirtor2016", writable=True)
    params = [f.stem.split("_")[:-1] for f in path.glob("*.dat")]

    # Parameters of SKIRTOR 2016
    t = list({param[0][1:] for param in params})
    p = list({param[1][1:] for param in params})
    q = list({param[2][1:] for param in params})
    oa = list({param[3][2:] for param in params})
    R = list({param[4][1:] for param in params})
    Mcl = list({param[5][3:] for param in params})
    i = list({param[6][1:] for param in params})

    iter_params = (
        (p1, p2, p3, p4, p5, p6, p7)
        for p1 in t
        for p2 in p
        for p3 in q
        for p4 in oa
        for p5 in R
        for p6 in Mcl
        for p7 in i
    )

    counter = Counter(len(t) * len(p) * len(q) * len(oa) * len(R) * len(Mcl) * len(i))
    for params in iter_params:
        filename = path / "t{}_p{}_q{}_oa{}_R{}_Mcl{}_i{}_sed.dat".format(*params)

        wl, disk, scatt, dust = np.genfromtxt(
            filename, unpack=True, usecols=(0, 2, 3, 4)
        )
        wl *= 1e3
        disk += scatt
        disk /= wl
        dust /= wl

        # Extrapolate the model to 10 mm
        wl_ext = np.array([2e6, 4e6, 8e6, 1e7])
        disk_ext = np.zeros(len(wl_ext)) + 1e-99
        if dust[-1] == 0:
            dust_ext = np.zeros(len(wl_ext)) + 1e-99
        else:
            dust_ext = 10 ** (
                np.log10(dust[-1])
                + np.log10(wl_ext / wl[-1])
                * np.log10(dust[-2] / dust[-1])
                / np.log10(wl[-2] / wl[-1])
            )
        wl = np.append(wl, wl_ext)
        disk[-1] = 1e-99
        disk = np.append(disk, disk_ext)
        dust = np.append(dust, dust_ext)

        # Interpolate to a denser grid
        with SimpleDatabase("nebular_continuum") as db1:
            nebular = db1.get(Z=0.019, logU=-2.0, ne=100.0)
        wl_den = nebular.wl[np.where((nebular.wl >= 3e4) & (nebular.wl <= 1e7))]
        idx = np.where(wl > 1e4)
        disk_den = 10 ** np.interp(
            np.log10(wl_den), np.log10(wl[idx]), np.log10(disk[idx])
        )
        dust_den = 10 ** np.interp(
            np.log10(wl_den), np.log10(wl[idx]), np.log10(dust[idx])
        )
        idx = np.where(wl < 3e4)
        wl = np.append(wl[idx], wl_den)
        disk = np.append(disk[idx], disk_den)
        dust = np.append(dust[idx], dust_den)

        # Normalization of the lumin_therm to 1W
        norm = np.trapz(dust, x=wl)
        disk /= norm
        dust /= norm

        db.add(
            {
                "t": int(params[0]),
                "pl": float(params[1]),
                "q": float(params[2]),
                "oa": int(params[3]),
                "R": int(params[4]),
                "Mcl": float(params[5]),
                "i": int(params[6]),
            },
            {"norm": norm, "wl": wl, "disk": disk, "dust": dust},
        )
        counter.inc()
    db.close()
    counter.progress.join()
