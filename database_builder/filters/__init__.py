"""This script is used the build pcigale internal database containing."""

from pathlib import Path

import numpy as np
import scipy.constants as cst

from pcigale.data import SimpleDatabase
from pcigale.utils.console import console
from pcigale.utils.counter import Counter


def build():
    console.rule("Photometric filters")

    path = Path(__file__).parent / "data"
    db = SimpleDatabase("filters", writable=True)

    files = list(path.glob("**/*.dat"))
    files += list(path.glob("**/*.pb"))

    counter = Counter(len(files))

    for file in files:
        with file.open() as f:
            name = f.readline().strip("# \n\t")
            type_ = f.readline().strip("# \n\t")
            if "gazpar" in str(file):
                _ = f.readline()  # We do not use the calib type
            desc = f.readline().strip("# \n\t")

        wl, tr = np.genfromtxt(file, unpack=True)

        # We convert the wavelength from Å to nm.
        wl *= 0.1

        # We convert to energy if needed
        if type_ == "photon":
            tr *= wl
        elif type_ != "energy":
            raise ValueError(
                "Filter transmission type can only be 'energy' or 'photon'."
            )

        # We normalise the filter and compute the pivot wavelength.
        pivot = np.sqrt(np.trapz(tr, wl) / np.trapz(tr / wl**2, wl))

        # The factor 10²⁰ is so that we get the fluxes directly in mJy when
        # we integrate with the wavelength in units of nm and the spectrum
        # in units of W/m²/nm.
        tr *= 1e20 / (cst.c * np.trapz(tr / wl**2, wl))
        db.add({"name": name}, {"wl": wl, "tr": tr, "pivot": pivot, "desc": desc})
        counter.inc()

    db.close()
    counter.progress.join()
