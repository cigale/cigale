"""This script is used the build pcigale internal database containing."""

import itertools
from pathlib import Path

import numpy as np
from scipy import interpolate

from pcigale.data import SimpleDatabase
from pcigale.utils.console import console
from pcigale.utils.counter import Counter


def read_bc03_ssp(filename):
    """Read a Bruzual and Charlot 2003 ASCII SSP file

    The ASCII SSP files of Bruzual and Charlot 2003 have se special structure.
    A vector is stored with the number of values followed by the values
    separated by a space (or a carriage return). There are the time vector, 5
    (for Chabrier IMF) or 6 lines (for Salpeter IMF) that we don't care of,
    then the wavelength vector, then the luminosity vectors, each followed by
    a 52 value table, then a bunch of other table of information that are also
    in the *colors files.

    Parameters
    ----------
    filename : Path

    Returns
    -------
    time_grid: numpy 1D array of floats
              Vector of the time grid of the SSP in Myr.
    wavelength: numpy 1D array of floats
                Vector of the wavelength grid of the SSP in nm.
    spectra: numpy 2D array of floats
             Array containing the SSP spectra, first axis is the wavelength,
             second one is the time.

    """

    def file_structure_generator():
        """Generator used to identify table lines in the SSP file

        In the SSP file, the vectors are store one next to the other, but
        there are 5 informational lines after the time vector. We use this
        generator to the if we are on lines to read or not.
        """
        if "chab" in filename.stem:
            bad_line_number = 5
        else:
            bad_line_number = 6
        yield ("data")
        for _ in range(bad_line_number):
            yield ("bad")
        while True:
            yield ("data")

    file_structure = file_structure_generator()
    # Are we in a data line or a bad one.
    what_line = next(file_structure)
    # Variable conting, in reverse order, the number of value still to
    # read for the read vector.
    counter = 0

    time_grid = []
    full_table = []
    tmp_table = []

    with filename.open() as file_:
        # We read the file line by line.
        for line in file_:
            if what_line == "data":
                # If we are in a "data" line, we analyse each number.
                for item in line.split():
                    if counter == 0:
                        # If counter is 0, then we are not reading a vector
                        # and the first number is the length of the next
                        # vector.
                        counter = int(item)
                    else:
                        # If counter > 0, we are currently reading a vector.
                        tmp_table.append(float(item))
                        counter -= 1
                        if counter == 0:
                            # We reached the end of the vector. If we have not
                            # yet store the time grid (the first table) we are
                            # currently reading it.
                            if time_grid == []:
                                time_grid = tmp_table[:]
                            # Else, we store the vector in the full table,
                            # only if its length is superior to 250 to get rid
                            # of the 52 item unknown vector and the 221 (time
                            # grid length) item vectors at the end of the
                            # file.
                            elif len(tmp_table) > 250:
                                full_table.append(tmp_table[:])

                            tmp_table = []

            # If at the end of a line, we have finished reading a vector, it's
            # time to change to the next structure context.
            if counter == 0:
                what_line = next(file_structure)

    # The time grid is in year, we want Myr.
    time_grid = np.array(time_grid, dtype=float)
    time_grid *= 1.0e-6

    # The first "long" vector encountered is the wavelength grid. The value
    # are in Ångström, we convert it to nano-meter.
    wavelength = np.array(full_table.pop(0), dtype=float)
    wavelength *= 0.1

    # The luminosities are in Solar luminosity (3.826.10^33 ergs.s-1) per
    # Ångström, we convert it to W/nm.
    luminosity = np.array(full_table, dtype=float)
    luminosity *= 3.826e27
    # Transposition to have the time in the second axis.
    luminosity = luminosity.transpose()

    # In the SSP, the time grid begins at 0, but not in the *colors file, so
    # we remove t=0 from the SSP.
    return time_grid[1:], wavelength, luminosity[:, 1:]


def build(res, agemax=13700):
    console.rule("Bruzual & Charlot (2003) single stellar populations")

    path = Path(__file__).parent / "data"
    db = SimpleDatabase("bc03", writable=True)

    # Time grid (1 Myr to agemax Gyr with 1 Myr step)
    agemax = int(agemax)
    time_grid = np.arange(1, agemax + 1)
    fine_time_grid = np.linspace(0.1, agemax, 10 * agemax)

    # Metallicities associated to each key
    metallicity = {
        "m22": 0.0001,
        "m32": 0.0004,
        "m42": 0.004,
        "m52": 0.008,
        "m62": 0.02,
        "m72": 0.05,
    }

    combinations = list(itertools.product(metallicity, ["salp", "chab"]))
    counter = Counter(len(combinations))

    for key, imf in combinations:
        ssp_filename = path / f"bc2003_{res}_{key}_{imf}_ssp.ised_ASCII"
        color3_filename = path / f"bc2003_lr_{key}_{imf}_ssp.3color"
        color4_filename = path / f"bc2003_lr_{key}_{imf}_ssp.4color"

        # Read the desired information from the color files
        color_table = []
        color3_table = np.genfromtxt(color3_filename).transpose()
        color4_table = np.genfromtxt(color4_filename).transpose()
        color_table.append(color4_table[6])  # Mstar
        color_table.append(color4_table[7])  # Mgas
        color_table.append(10 ** color3_table[5])  # NLy

        color_table = np.array(color_table)

        ssp_time, ssp_wave, ssp_lumin = read_bc03_ssp(ssp_filename)

        # Regrid the SSP data to the evenly spaced time grid. In doing so we
        # assume 10 bursts every 0.1 Myr over a period of 1 Myr in order to
        # capture short evolutionary phases.
        # The time grid starts after 0.1 Myr, so we assume the value is the same
        # as the first actual time step.
        fill_value = (color_table[:, 0], color_table[:, -1])
        color_table = interpolate.interp1d(
            ssp_time,
            color_table,
            fill_value=fill_value,
            bounds_error=False,
            assume_sorted=True,
        )(fine_time_grid)
        color_table = np.mean(color_table.reshape(3, -1, 10), axis=-1)

        # We have to do the interpolation-averaging in several blocks as it is
        # a bit RAM intensive
        ssp_lumin_interp = np.empty((ssp_wave.size, time_grid.size))
        for i in range(0, ssp_wave.size, 100):
            fill_value = (ssp_lumin[i : i + 100, 0], ssp_lumin[i : i + 100, -1])
            ssp_interp = interpolate.interp1d(
                ssp_time,
                ssp_lumin[i : i + 100, :],
                fill_value=fill_value,
                bounds_error=False,
                assume_sorted=True,
            )(fine_time_grid)
            ssp_interp = ssp_interp.reshape(ssp_interp.shape[0], -1, 10)
            ssp_lumin_interp[i : i + 100, :] = np.mean(ssp_interp, axis=-1)

        # To avoid the creation of waves when interpolating, we refine the grid
        # beyond 10 μm following a log scale in wavelength. The interpolation
        # is also done in log space as the spectrum is power-law-like
        ssp_wave_resamp = np.around(np.logspace(np.log10(10000), np.log10(160000), 50))
        argmin = np.argmin(10000.0 - ssp_wave > 0) - 1
        ssp_lumin_resamp = 10.0 ** interpolate.interp1d(
            np.log10(ssp_wave[argmin:]),
            np.log10(ssp_lumin_interp[argmin:, :]),
            assume_sorted=True,
            axis=0,
        )(np.log10(ssp_wave_resamp))

        ssp_wave = np.hstack([ssp_wave[: argmin + 1], ssp_wave_resamp])
        ssp_lumin = np.vstack([ssp_lumin_interp[: argmin + 1, :], ssp_lumin_resamp])

        db.add(
            {"imf": imf, "Z": metallicity[key]},
            {"t": time_grid, "wl": ssp_wave, "info": color_table, "spec": ssp_lumin},
        )
        counter.inc()
    db.close()
    counter.progress.join()
