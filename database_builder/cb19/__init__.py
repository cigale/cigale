"""This script is used the build pcigale internal database containing."""

import itertools
from pathlib import Path
import warnings

from astropy.table import Table
from astropy.utils.exceptions import AstropyWarning
import numpy as np
from scipy import interpolate

from pcigale.data import SimpleDatabase
from pcigale.utils.console import WARNING, console
from pcigale.utils.counter import Counter


def build():
    console.rule("Charlot & Bruzual (2019) single stellar populations")

    path = Path(__file__).parent / "data"
    db = SimpleDatabase("cb19", writable=True)

    # Time grid (1 Myr to 14 Gyr with 1 Myr step)
    time_grid = np.arange(1, 13700)
    fine_time_grid = np.linspace(0.1, 13699, 136990)

    # Metallicities associated to each key
    metal = {
        "0001": 0.0001,
        "0002": 0.0002,
        "0005": 0.0005,
        "001": 0.001,
        "002": 0.002,
        "004": 0.004,
        "006": 0.006,
        "008": 0.008,
        "010": 0.01,
        "014": 0.014,
        "017": 0.017,
        "020": 0.02,
        "030": 0.03,
        "040": 0.04,
        "060": 0.06,
    }

    # IMF associated to each key
    imf = {
        "salp": 0,
        "chab": 1,
        "kroup": 2,
    }

    # Upper limit of the stellar mass
    MU = {"": 100, "_MU300": 300, "_MU600": 600}

    with SimpleDatabase("bc03") as bc03db:
        ssp_wave_lr = bc03db.get(imf="salp", Z=0.02).wl
    ssp_wave_lr = ssp_wave_lr

    nfiles = len(list(path.rglob("cb2019*.fits")))
    counter = Counter(nfiles)
    if nfiles > 0:
        warnings.simplefilter("ignore", AstropyWarning)
        for key_metal, key_imf, key_MU in itertools.product(metal, imf, MU):
            fname = path / f"cb2019_z{key_metal}_{key_imf}{key_MU}_hr_xmilesi_ssp.fits"

            # As CB19 models are very large, it is unlikely that they are all
            # available, so we skips the models that are not present
            if not Path(fname).is_file():
                continue

            t_spec = Table.read(fname, hdu=1)
            t_info = Table.read(fname, hdu=2)
            t_age = Table.read(fname, hdu=5)

            ssp_time = t_age["age-yr"].data * 1e-6
            ssp_wave = t_spec["Wavelength"].data * 0.1
            ssp_lumin = (
                Table(t_spec.columns[1:]).as_array().view("f8").reshape(len(t_spec), -1)
            )

            # Interpolate on the BC03 models to reduce the wavelength sampling
            ssp_lumin = 10 ** interpolate.interp1d(
                np.log10(ssp_wave), np.log10(ssp_lumin), axis=0, assume_sorted=True
            )(np.log10(ssp_wave_lr))
            ssp_lumin[np.where(np.isnan(ssp_lumin))] = 0.0
            ssp_wave = ssp_wave_lr

            # The luminosities are in Solar luminosity (3.826×10²⁶ W) per
            # Ångström, we convert it to W/nm
            ssp_lumin *= 3.826e27

            color_table = np.array(
                [
                    10.0 ** t_info["logNLy"].data.astype(np.float64),
                    (t_info["Mstars"] + t_info["Mremnants"]).data.astype(np.float64),
                ]
            )

            # Regrid the SSP data to the evenly spaced time grid. In doing so we
            # assume 10 bursts every 0.1 Myr over a period of 1 Myr in order to
            # capture short evolutionary phases.
            # The time grid starts after 0.1 Myr, so we assume the value is the same
            # as the first actual time step.
            fill_value = (color_table[:, 0], color_table[:, -1])
            color_table = interpolate.interp1d(
                ssp_time,
                color_table,
                fill_value=fill_value,
                bounds_error=False,
                assume_sorted=True,
            )(fine_time_grid)
            color_table = np.mean(color_table.reshape(2, -1, 10), axis=-1)

            # We have to do the interpolation-averaging in several blocks as it is
            # a bit RAM intensive
            ssp_lumin_interp = np.empty((ssp_wave.size, time_grid.size))
            for i in range(0, ssp_wave.size, 100):
                fill_value = (ssp_lumin[i : i + 100, 0], ssp_lumin[i : i + 100, -1])
                ssp_interp = interpolate.interp1d(
                    ssp_time,
                    ssp_lumin[i : i + 100, :],
                    fill_value=fill_value,
                    bounds_error=False,
                    assume_sorted=True,
                )(fine_time_grid)
                ssp_interp = ssp_interp.reshape(ssp_interp.shape[0], -1, 10)
                ssp_lumin_interp[i : i + 100, :] = np.mean(ssp_interp, axis=-1)

            # To avoid the creation of waves when interpolating, we refine the grid
            # beyond 10 μm following a log scale in wavelength. The interpolation
            # is also done in log space as the spectrum is power-law-like
            ssp_wave_resamp = np.around(
                np.logspace(np.log10(10000), np.log10(160000), 50)
            )
            argmin = np.argmin(10000.0 - ssp_wave > 0) - 1
            ssp_lumin_resamp = 10.0 ** interpolate.interp1d(
                np.log10(ssp_wave[argmin:]),
                np.log10(ssp_lumin_interp[argmin:, :]),
                assume_sorted=True,
                axis=0,
            )(np.log10(ssp_wave_resamp))

            ssp_wave = np.hstack([ssp_wave[: argmin + 1], ssp_wave_resamp])
            ssp_lumin = np.vstack([ssp_lumin_interp[: argmin + 1, :], ssp_lumin_resamp])

            db.add(
                {"imf": imf[key_imf], "Z": metal[key_metal], "upper": MU[key_MU]},
                {
                    "t": time_grid,
                    "wl": ssp_wave,
                    "info": color_table,
                    "spec": ssp_lumin,
                },
            )
            counter.inc()
    else:
        console.print(f"{WARNING} No CB19 model found")

    db.close()
    counter.progress.join()
