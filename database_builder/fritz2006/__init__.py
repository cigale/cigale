"""This script is used the build pcigale internal database containing."""

import io
from pathlib import Path

import numpy as np

from pcigale.data import SimpleDatabase
from pcigale.utils.console import console
from pcigale.utils.counter import Counter


def build():
    console.rule("Fritz (2006) AGN models")

    path = Path(__file__).parent / "data"
    db = SimpleDatabase("fritz2006", writable=True)

    # Parameters of Fritz+2006
    psy = [
        "0.001",
        "10.100",
        "20.100",
        "30.100",
        "40.100",
        "50.100",
        "60.100",
        "70.100",
        "80.100",
        "89.990",
    ]  # Viewing angle in degrees
    opening_angle = [20, 40, 60]  # Theta = 2*(90 - opening_angle)
    gamma = ["0.0", "2.0", "4.0", "6.0"]
    beta = ["-1.00", "-0.75", "-0.50", "-0.25", "0.00"]
    tau = ["0.1", "0.3", "0.6", "1.0", "2.0", "3.0", "6.0", "10.0"]
    r_ratio = [10, 30, 60, 100, 150]

    # Read and convert the wavelength
    filename = path / "ct20al0.0be-1.00ta0.1rm10.tot"
    with filename.open() as datafile:
        data = "".join(datafile.readlines()[-178:])
    wave = np.genfromtxt(io.BytesIO(data.encode()), usecols=(0))
    wave *= 1e3
    # Number of wavelengths: 178; Number of comments lines: 28
    nskip = 28
    blocksize = 178

    iter_params = (
        (oa, gam, be, ta, rm)
        for oa in opening_angle
        for gam in gamma
        for be in beta
        for ta in tau
        for rm in r_ratio
    )

    counter = Counter(
        len(opening_angle) * len(gamma) * len(beta) * len(tau) * len(r_ratio) * len(psy)
    )
    for params in iter_params:
        filename = path / "ct{}al{}be{}ta{}rm{}.tot".format(*params)
        try:
            with filename.open() as datafile:
                data = datafile.readlines()
        except IOError:
            continue

        for n in range(len(psy)):
            block = data[
                nskip
                + blocksize * n
                + 4 * (n + 1)
                - 1 : nskip
                + blocksize * (n + 1)
                + 4 * (n + 1)
                - 1
            ]
            dust, scatt, disk = np.genfromtxt(
                io.BytesIO("".join(block).encode()), usecols=(2, 3, 4), unpack=True
            )
            # Remove NaN
            dust = np.nan_to_num(dust)
            scatt = np.nan_to_num(scatt)
            disk = np.nan_to_num(disk)
            # Merge scatter into disk
            disk += scatt
            # Conversion from erg/s/microns to W/nm
            dust *= 1e-4
            disk *= 1e-4
            # Normalization of the lumin_therm to 1W
            norm = np.trapz(dust, x=wave)
            dust /= norm
            disk /= norm

            db.add(
                {
                    "r_ratio": float(params[4]),
                    "tau": float(params[3]),
                    "beta": float(params[2]),
                    "gamma": float(params[1]),
                    "opening_angle": float(params[0]),
                    "psy": float(psy[n]),
                },
                {"norm": norm, "wl": wave, "disk": disk, "dust": dust},
            )
            counter.inc()
    db.close()
    counter.progress.join()
