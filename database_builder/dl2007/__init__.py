"""This script is used the build pcigale internal database containing."""

import io
from pathlib import Path

import numpy as np
import scipy.constants as cst

from pcigale.data import SimpleDatabase
from pcigale.utils.console import console
from pcigale.utils.counter import Counter


def build():
    console.rule("Draine & Li (2007) dust models")

    path = Path(__file__).parent / "data"
    db = SimpleDatabase("dl2007", writable=True)

    qpah = {
        "00": 0.47,
        "10": 1.12,
        "20": 1.77,
        "30": 2.50,
        "40": 3.19,
        "50": 3.90,
        "60": 4.58,
    }

    umaximum = ["1e3", "1e4", "1e5", "1e6"]
    uminimum = [
        "0.10",
        "0.15",
        "0.20",
        "0.30",
        "0.40",
        "0.50",
        "0.70",
        "0.80",
        "1.00",
        "1.20",
        "1.50",
        "2.00",
        "2.50",
        "3.00",
        "4.00",
        "5.00",
        "7.00",
        "8.00",
        "10.0",
        "12.0",
        "15.0",
        "20.0",
        "25.0",
    ]

    # Mdust/MH used to retrieve the dust mass as models as given per atom of H
    MdMH = {
        "00": 0.0100,
        "10": 0.0100,
        "20": 0.0101,
        "30": 0.0102,
        "40": 0.0102,
        "50": 0.0103,
        "60": 0.0104,
    }

    # Here we obtain the wavelength beforehand to avoid reading it each time.
    filename = path / "U1e3" / "U1e3_1e3_MW3.1_00.txt"
    with filename.open() as datafile:
        data = "".join(datafile.readlines()[-1001:])

    wave = np.genfromtxt(io.BytesIO(data.encode()), usecols=(0))
    # For some reason wavelengths are decreasing in the model files
    wave = wave[::-1]
    # We convert wavelengths from μm to nm
    wave *= 1000.0

    # Conversion factor from Jy cm² sr¯¹ H¯¹ to W nm¯¹ (kg of H)¯¹
    conv = 4.0 * np.pi * 1e-30 / (cst.m_p + cst.m_e) * cst.c / (wave * wave) * 1e9

    counter = Counter(len(qpah) * (len(umaximum) + 1) * len(uminimum))
    for model in qpah.keys():
        for umin in uminimum:
            filename = path / f"U{umin}" / f"U{umin}_{umin}_MW3.1_{model}.txt"
            with filename.open() as datafile:
                data = "".join(datafile.readlines()[-1001:])
            lumin = np.genfromtxt(io.BytesIO(data.encode()), usecols=(2))
            # For some reason fluxes are decreasing in the model files
            lumin = lumin[::-1]
            # Conversion from Jy cm² sr¯¹ H¯¹to W nm¯¹ (kg of dust)¯¹
            lumin *= conv / MdMH[model]

            db.add(
                {"qpah": float(qpah[model]), "umin": float(umin), "umax": float(umin)},
                {"wl": wave, "spec": lumin},
            )
            counter.inc()

            for umax in umaximum:
                filename = path / f"U{umin}" / f"U{umin}_{umax}_MW3.1_{model}.txt"
                with filename.open() as datafile:
                    data = "".join(datafile.readlines()[-1001:])
                lumin = np.genfromtxt(io.BytesIO(data.encode()), usecols=(2))
                # For some reason fluxes are decreasing in the model files
                lumin = lumin[::-1]

                # Conversion from Jy cm² sr¯¹ H¯¹to W nm¯¹ (kg of dust)¯¹
                lumin *= conv / MdMH[model]

                db.add(
                    {
                        "qpah": float(qpah[model]),
                        "umin": float(umin),
                        "umax": float(umax),
                    },
                    {"wl": wave, "spec": lumin},
                )
                counter.inc()
    db.close()
    counter.progress.join()
