"""This script is used the build pcigale internal database."""

from pathlib import Path

from .bc03 import build as build_bc2003
from .bpassv2p2 import build as build_bpassv2
from .cb19 import build as build_cb2019
from .dale2014 import build as build_dale2014
from .dl2007 import build as build_dl2007
from .dl2014 import build as build_dl2014
from .filters import build as build_filters
from .fritz2006 import build as build_fritz2006
from .maraston2005 import build as build_m2005
from .nebular import build as build_nebular
from .schreiber2016 import build as build_schreiber2016
from .skirtor2016 import build as build_skirtor2016
from .themis import build as build_themis
from pcigale.utils.info import Info


def check_lfs():
    filename = Path(__file__).parent / "nebular" / "data" / "continuum.dat"

    if filename.lstat().st_size < 1024:
        raise Exception(
            "It is likely that git-lfs is not installed. Install and set it "
            "up before cloning the repository. https://git-lfs.com/"
        )


def build_base(bc03res="lr", bpassres="lr", bpassresmax=None, agemax=13700):
    Info.print_panel()

    check_lfs()

    build_filters()
    build_m2005(agemax)
    build_bc2003(bc03res, agemax)
    build_bpassv2(bpassres, bpassresmax, agemax)
    build_cb2019()
    build_dl2007()
    build_dl2014()
    build_themis()
    build_dale2014()
    build_schreiber2016()
    build_nebular()
    build_fritz2006()
    build_skirtor2016()


if __name__ == "__main__":
    build_base()
