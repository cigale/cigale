"""This script is used the build pcigale internal database containing."""

import io
from pathlib import Path

import numpy as np

from pcigale.data import SimpleDatabase
from pcigale.utils.console import console
from pcigale.utils.counter import Counter


def build():
    console.rule("Dale (2014) dust models")

    path = Path(__file__).parent / "data"
    db = SimpleDatabase("dale2014", writable=True)

    # Getting the alpha grid for the templates
    d14cal = np.genfromtxt(path / "dhcal.dat")
    alpha_grid = d14cal[:, 1]

    # Getting the lambda grid for the templates and convert from microns to nm.
    first_template = np.genfromtxt(path / "spectra.0.00AGN.dat")
    wave = first_template[:, 0] * 1e3

    # Getting the stellar emission and interpolate it at the same wavelength
    # grid
    stell_emission_file = np.genfromtxt(path / "stellar_SED_age13Gyr_tau10Gyr.spec")
    # A -> to nm
    wave_stell = stell_emission_file[:, 0] * 0.1
    # W/A -> W/nm
    stell_emission = stell_emission_file[:, 1] * 10
    stell_emission_interp = np.interp(wave, wave_stell, stell_emission)

    # The models are in nuFnu and contain stellar emission.
    # We convert this to W/nm and remove the stellar emission.

    # Emission from dust heated by SB
    fraction = 0.0
    filename = path / "spectra.0.00AGN.dat"
    with filename.open() as datafile:
        data = "".join(datafile.readlines())

    counter = Counter(len(alpha_grid) + 1)
    for al in range(1, len(alpha_grid) + 1, 1):
        lumin_with_stell = np.genfromtxt(io.BytesIO(data.encode()), usecols=(al))
        lumin_with_stell = pow(10, lumin_with_stell) / wave
        constant = lumin_with_stell[7] / stell_emission_interp[7]
        lumin = lumin_with_stell - stell_emission_interp * constant
        lumin[lumin < 0] = 0
        lumin[wave < 2e3] = 0
        norm = np.trapz(lumin, x=wave)
        lumin /= norm

        db.add(
            {"fracAGN": float(fraction), "alpha": float(alpha_grid[al - 1])},
            {"wl": wave, "spec": lumin},
        )
        counter.inc()

    # Emission from dust heated by AGN - Quasar template
    filename = path / "shi_agn.regridded.extended.dat"

    wave, lumin_quasar = np.genfromtxt(filename, unpack=True)
    wave *= 1e3
    lumin_quasar = 10**lumin_quasar / wave
    norm = np.trapz(lumin_quasar, x=wave)
    lumin_quasar /= norm

    db.add({"fracAGN": 1.0, "alpha": 0.0}, {"wl": wave, "spec": lumin_quasar})
    counter.inc()

    db.close()
    counter.progress.join()
