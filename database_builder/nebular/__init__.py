"""This script is used the build pcigale internal database containing."""

from pathlib import Path

from astropy.table import Table
import numpy as np
import scipy.constants as cst
from scipy import interpolate

from pcigale.data import SimpleDatabase
from pcigale.utils.console import console
from pcigale.utils.counter import Counter


def build():
    path = Path(__file__).parent / "data"

    filename = path / "lines.dat"
    lines = np.genfromtxt(filename)

    tmp = Table.read(path / "line_wavelengths.dat", format="ascii")
    wave_lines = tmp["col1"].data
    name_lines = tmp["col2"].data

    # Build the parameters
    metallicities = np.unique(lines[:, 1])
    logUs = np.around(np.arange(-4.0, -0.9, 0.1), 1)
    nes = np.array([10.0, 100.0, 1000.0])

    filename = path / "continuum.dat"
    cont = np.genfromtxt(filename)

    # Convert wavelength from Å to nm
    wave_lines *= 0.1
    wave_cont = cont[:1600, 0] * 0.1

    # Compute the wavelength grid to resample the models so as to eliminate
    # non-physical waves and compute the models faster by avoiding resampling
    # them at run time.
    with SimpleDatabase("bc03") as db:
        wave_stellar = db.get(imf="salp", Z=0.02).wl
    with SimpleDatabase("dl2014") as db:
        wave_dust = db.get(qpah=0.47, umin=1.0, umax=1.0, alpha=1.0).wl
    wave_cont_interp = np.unique(
        np.hstack([wave_cont, wave_stellar, wave_dust, np.logspace(7.0, 9.0, 501)])
    )

    # Keep only the fluxes
    lines = lines[:, 2:]
    cont = cont[:, 1:]

    # Reshape the arrays so they are easier to handle
    cont = np.reshape(cont, (metallicities.size, wave_cont.size, logUs.size, nes.size))
    lines = np.reshape(
        lines, (wave_lines.size, metallicities.size, logUs.size, nes.size)
    )

    # Move the wavelength to the last position to ease later computations
    # 0: metallicity, 1: log U, 2: ne, 3: wavelength
    cont = np.moveaxis(cont, 1, -1)
    lines = np.moveaxis(lines, (0, 1, 2, 3), (3, 0, 1, 2))

    # Convert lines to a linear scale
    lines = 10.0**lines

    # Convert continuum to W/nm
    cont *= 1e-7 * cst.c * 1e9 / wave_cont**2

    # Import lines
    db = SimpleDatabase("nebular_lines", writable=True)

    console.rule("Nebular line models")
    counter = Counter(len(metallicities) * len(logUs) * len(nes))
    for idxZ, metallicity in enumerate(metallicities):
        for idxU, logU in enumerate(logUs):
            for ne, spectrum in zip(nes, lines[idxZ, idxU, :, :]):
                db.add(
                    {"Z": float(metallicity), "logU": float(logU), "ne": float(ne)},
                    {"name": name_lines, "wl": wave_lines, "spec": spectrum},
                )
                counter.inc()
    db.close()
    counter.progress.join()

    # Import continuum
    db = SimpleDatabase("nebular_continuum", writable=True)
    spectra = 10 ** interpolate.interp1d(np.log10(wave_cont), np.log10(cont), axis=-1)(
        np.log10(wave_cont_interp)
    )
    spectra = np.nan_to_num(spectra)

    console.rule("Nebular continuum models")
    counter = Counter(len(metallicities) * len(logUs) * len(nes))
    for idxZ, metallicity in enumerate(metallicities):
        for idxU, logU in enumerate(logUs):
            for ne, spectrum in zip(nes, spectra[idxZ, idxU, :, :]):
                db.add(
                    {"Z": float(metallicity), "logU": float(logU), "ne": float(ne)},
                    {"wl": wave_cont_interp, "spec": spectrum},
                )
                counter.inc()
    db.close()
    counter.progress.join()
