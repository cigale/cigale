"""This script is used the build pcigale internal database containing."""

from pathlib import Path

import numpy as np
from scipy import interpolate

from pcigale.data import SimpleDatabase
from pcigale.utils.console import console
from pcigale.utils.counter import Counter



def build(agemax=13700):
    console.rule("Maraston (2005) single stellar populations")

    path = Path(__file__).parent / "data"
    db = SimpleDatabase("m2005", writable=True)

    # Age grid (1 Myr to agemax Myr with 1 Myr step)
    agemax = int(agemax)
    time_grid = np.arange(1, agemax + 1)
    fine_time_grid = np.linspace(0.1, agemax, 10 * agemax)

    # Transpose the table to have access to each value vector on the first
    # axis
    kroupa_mass = np.genfromtxt(path / "stellarmass.kroupa").transpose()
    salpeter_mass = np.genfromtxt(path / "stellarmass.salpeter").transpose()

    files = list(path.glob("*.rhb"))
    counter = Counter(len(files))

    for spec_file in files:
        spec_table = np.genfromtxt(spec_file).transpose()
        metallicity = spec_table[1, 0]

        if "krz" in spec_file.stem:
            imf = "krou"
            mass_table = np.copy(kroupa_mass)
        elif "ssz" in spec_file.stem:
            imf = "salp"
            mass_table = np.copy(salpeter_mass)
        else:
            raise ValueError("Unknown IMF!!!")

        # Keep only the actual metallicity values in the mass table
        # we don't take the first column which contains metallicity.
        # We also eliminate the turn-off mas which makes no send for composite
        # populations.
        mass_table = mass_table[1:7, mass_table[0] == metallicity]

        # Regrid the SSP data to the evenly spaced time grid. In doing so we
        # assume 10 bursts every 0.1 Myr over a period of 1 Myr in order to
        # capture short evolutionary phases.
        # The time grid starts after 0.1 Myr, so we assume the value is the same
        # as the first actual time step.
        mass_table = interpolate.interp1d(
            mass_table[0] * 1e3, mass_table[1:], assume_sorted=True
        )(fine_time_grid)
        mass_table = np.mean(mass_table.reshape(5, -1, 10), axis=-1)

        # Extract the age and convert from Gyr to Myr
        ssp_time = np.unique(spec_table[0]) * 1e3
        spec_table = spec_table[1:]

        # Remove the metallicity column from the spec table
        spec_table = spec_table[1:]

        # Extract the wavelength and convert from Å to nm
        ssp_wave = spec_table[0][:1221] * 0.1
        spec_table = spec_table[1:]

        # Extra the fluxes and convert from erg/s/Å to W/nm
        ssp_lumin = spec_table[0].reshape(ssp_time.size, ssp_wave.size).T
        ssp_lumin *= 10 * 1e-7

        # We have to do the interpolation-averaging in several blocks as it is
        # a bit RAM intensive
        ssp_lumin_interp = np.empty((ssp_wave.size, time_grid.size))
        for i in range(0, ssp_wave.size, 100):
            fill_value = (ssp_lumin[i : i + 100, 0], ssp_lumin[i : i + 100, -1])
            ssp_interp = interpolate.interp1d(
                ssp_time,
                ssp_lumin[i : i + 100, :],
                fill_value=fill_value,
                bounds_error=False,
                assume_sorted=True,
            )(fine_time_grid)
            ssp_interp = ssp_interp.reshape(ssp_interp.shape[0], -1, 10)
            ssp_lumin_interp[i : i + 100, :] = np.mean(ssp_interp, axis=-1)

        # To avoid the creation of waves when interpolating, we refine the grid
        # beyond 10 μm following a log scale in wavelength. The interpolation
        # is also done in log space as the spectrum is power-law-like
        ssp_wave_resamp = np.around(np.logspace(np.log10(10000), np.log10(160000), 50))
        argmin = np.argmin(10000.0 - ssp_wave > 0) - 1
        ssp_lumin_resamp = 10.0 ** interpolate.interp1d(
            np.log10(ssp_wave[argmin:]),
            np.log10(ssp_lumin_interp[argmin:, :]),
            assume_sorted=True,
            axis=0,
        )(np.log10(ssp_wave_resamp))

        ssp_wave = np.hstack([ssp_wave[: argmin + 1], ssp_wave_resamp])
        ssp_lumin = np.vstack([ssp_lumin_interp[: argmin + 1, :], ssp_lumin_resamp])

        # Use Z value for metallicity, not log([Z/H])
        metallicity = {-1.35: 0.001, -0.33: 0.01, 0.0: 0.02, 0.35: 0.04}[metallicity]

        db.add(
            {"imf": imf, "Z": metallicity},
            {"t": time_grid, "wl": ssp_wave, "info": mass_table, "spec": ssp_lumin},
        )
        counter.inc()
    db.close()
    counter.progress.join()
