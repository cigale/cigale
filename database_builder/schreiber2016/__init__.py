"""This script is used the build pcigale internal database containing."""

from pathlib import Path

from astropy.table import Table
import numpy as np

from pcigale.data import SimpleDatabase
from pcigale.utils.console import console
from pcigale.utils.counter import Counter


def build():
    console.rule("Schreiber (2016) dust models")

    path = Path(__file__).parent / "data"
    db = SimpleDatabase("schreiber2016", writable=True)

    filename = path / "g15_pah.fits"
    pah = Table.read(filename)

    filename = path / "g15_dust.fits"
    dust = Table.read(filename)

    # Getting the lambda grid for the templates and convert from μm to nm.
    wave = dust["LAM"][0, 0, :].data * 1e3

    counter = Counter(170)
    for td in np.arange(15.0, 100.0):
        # Find the closest temperature in the model list of tdust
        tsed = np.argmin(np.absolute(dust["TDUST"][0].data - td))

        # The models are in νFν.  We convert this to W/nm.
        lumin_dust = dust["SED"][0, tsed, :].data / wave
        lumin_pah = pah["SED"][0, tsed, :].data / wave

        db.add({"type": 0, "tdust": float(td)}, {"wl": wave, "spec": lumin_dust})
        counter.inc()
        db.add({"type": 1, "tdust": float(td)}, {"wl": wave, "spec": lumin_pah})
        counter.inc()

    db.close()
    counter.progress.join()
