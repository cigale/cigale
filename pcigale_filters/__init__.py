import argparse
import sys

from matplotlib import colormaps as cm
import matplotlib.pyplot as plt
import numpy as np
from rich import box
from rich.table import Table
from rich.text import Text
import scipy.constants as cst

from pcigale.data import SimpleDatabase as Database
from pcigale.utils.console import console


def print_filters(observatory, filters, strings):
    table = Table(title=observatory, style="panel", box=box.ROUNDED)
    table.add_column("Filter name")
    table.add_column("Description")
    table.add_column("Wavelength")

    if len(filters) > 0:
        n = filters[0].name.count(".")
    else:
        n = 0

    if n == 0:  # Orphaned filters
        names = [k.name for k in filters]
    elif n == 1:  # No instrument provided
        names = list(
            (f"{observatory}.{k.name.split('.')[1]}" for k in filters if "." in k.name)
        )
    else:
        names = list(
            {
                f"{observatory}.{'.'.join(k.name.split('.')[1:n])}"
                for k in filters
                if "." in k.name
            }
        )
        names.sort()

    for name in names:
        if n > 1:
            table.add_section()
        for k in filters:
            if k.name.startswith(name):
                splitdesc = k.desc.split()
                if splitdesc[-1].startswith("http"):
                    desc = f"[link={splitdesc[-1]}]{' '.join(splitdesc[:-1])}[/link]"
                else:
                    desc = k.desc
                if k.pivot < 1e3:
                    wl = f"{k.pivot:6.02f} nm"
                elif k.pivot < 1e6:
                    wl = f"{k.pivot * 1e-3:6.02f} μm"
                elif k.pivot < 1e7:
                    wl = f"{k.pivot * 1e-6:6.02f} mm"
                else:
                    wl = f"{k.pivot * 1e-7:6.02f} cm"
                fname = Text(k.name)
                fname.highlight_words(strings, "highlight")
                table.add_row(fname, desc, wl)

    console.print(table)
    console.print()


def print_lines(lines, strings):
    table = Table(title="lines", style="panel", box=box.ROUNDED)
    table.add_column("Line name")
    table.add_column("Wavelength")

    for name, wl in lines:
        if wl < 1e3:
            wl = f"{wl:6.02f} nm"
        elif wl < 1e6:
            wl = f"{wl * 1e-3:6.02f} μm"
        elif wl < 1e7:
            wl = f"{wl * 1e-6:6.02f} mm"
        else:
            wl = f"{wl * 1e-7:6.02f} cm"
        lname = Text(f"line.{name}")
        lname.highlight_words(strings, "highlight")
        table.add_row(lname, wl)

    console.print(table)
    console.print()


def list_filters(strings):
    """Print the list of filters in the pcigale database"""
    with Database("nebular_lines") as base:
        lines = base.get(Z=0.02, logU=-2.0, ne=100.0)
    lines = [
        (name, wl)
        for name, wl in sorted(zip(lines.name, lines.wl), key=lambda ele: ele[1])
    ]
    if len(strings) > 0:
        lines = [l for l in lines if any(s in l[0] for s in strings)]

    if len(lines) > 0:
        print_lines(lines, strings)
    else:
        console.print("No corresponding line found\n", style="data")

    console.rule()
    console.print()

    with Database("filters") as base:
        filters = {name: base.get(name=name) for name in base.parameters["name"]}
    if len(strings) > 0:
        filters = {k: v for k, v in filters.items() if any(s in k for s in strings)}
    if len(filters) > 0:
        observatories = list({k.split(".")[0] for k in filters if "." in k})
        observatories.sort()

        for observatory in observatories:
            obsfilters = [
                v
                for k, v in sorted(filters.items(), key=lambda ele: ele[1].pivot)
                if k.startswith(observatory)
            ]
            print_filters(observatory, obsfilters, strings)

        orphans = list({k for k in filters if "." not in k})
        orphans = [
            v
            for k, v in sorted(filters.items(), key=lambda ele: ele[1].pivot)
            if "." not in k
        ]
        if len(orphans) > 0:
            orphans.sort()
            print_filters("Unidentified observatory", orphans, strings)
    else:
        console.print("No corresponding filter found", style="data")


def add_filters(fnames):
    """Add filters to the pcigale database."""
    db = Database("filters", writable=True)

    for fname in fnames:
        with open(fname, "r") as f:
            name = f.readline().strip("# \n\t")
            type_ = f.readline().strip("# \n\t")
            desc = f.readline().strip("# \n\t")
        wl, tr = np.genfromtxt(fname, unpack=True)

        # We convert the wavelength from Å to nm.
        wl *= 0.1

        # We convert to energy if needed
        if type_ == "photon":
            tr *= wl
        elif type_ != "energy":
            raise ValueError(
                "Filter transmission type can only be 'energy' or 'photon'."
            )

        print(f"Importing {name}... ({wl.size} points)")

        # We normalise the filter and compute the pivot wavelength.
        pivot = np.sqrt(np.trapz(tr, wl) / np.trapz(tr / wl**2, wl))

        # The factor 10²⁰ is so that we get the fluxes directly in mJy when
        # we integrate with the wavelength in units of nm and the spectrum
        # in units of W/m²/nm.
        tr *= 1e20 / (cst.c * np.trapz(tr / wl**2, wl))

        db.add({"name": name}, {"wl": wl, "tr": tr, "pivot": pivot, "desc": desc})
    db.close()


def plot_filters(names):
    fname = names[0]
    with Database("filters") as db:
        if len(names) == 1:  # Get all filters containing that string
            names = [name for name in db.parameters["name"] if names[0] in name]
        filters = [db.get(name=name) for name in names]
    filters = [f for f in sorted(filters, key=lambda ele: ele.pivot)]

    # Select the unit based on the mean pivot wavelength of the filter set
    mean_pivot = np.mean([f.pivot for f in filters])
    if mean_pivot < 1e3:
        scale = 1.0
        unit = "nm"
    elif mean_pivot < 1e6:
        scale = 1e-3
        unit = "μm"
    elif mean_pivot < 1e7:
        scale = 1e-6
        unit = "mm"
    else:
        scale = 1e-7
        unit = "cm"

    cmap = cm.get_cmap("rainbow")
    plt.figure()
    n = len(filters) - 1
    if n > 0:
        for i, f in enumerate(filters):
            plt.fill_between(
                f.wl * scale,
                0.0,
                f.tr / np.max(f.tr),
                label=f.name,
                color=cmap(i / n),
                alpha=0.25,
            )
    else:
        f = filters[0]
        plt.fill_between(
            f.wl * scale,
            0.0,
            f.tr / np.max(f.tr),
            label=f.name,
            color="k",
            alpha=0.25,
        )
    plt.minorticks_on()
    plt.xlabel(f"Wavelength [{unit}]")
    plt.ylabel("Relative transmission [energy]")
    plt.tight_layout()

    # Adapt the number of columns to display all the filters in a single figure
    legend = plt.legend(loc="upper right", fontsize="xx-small")
    legend_bbox = legend.get_window_extent().transformed(plt.gca().transData.inverted())
    ncols = np.ceil(legend_bbox.height)
    if ncols > 1:
        legend = plt.legend(loc="upper right", ncols=ncols, fontsize="xx-small")
        legend_bbox = legend.get_window_extent().transformed(
            plt.gca().transData.inverted()
        )

    # If the legend takes more than 50% of the width, do not render
    xmin, xmax = plt.xlim()
    dx = xmax - xmin
    if legend_bbox.width > 0.5 * dx:
        raise Exception("Too many filters, the plot cannot be rendered correctly")

    # Adapt the width of the figure so that legend and data do not overlap
    plt.xlim(xmin, xmin + dx / (1.0 - legend_bbox.width / dx))
    plt.ylim(0.0, 1.0)

    plt.savefig(f"{fname}.pdf")
    plt.close()


def main():
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(help="List of commands")

    list_parser = subparsers.add_parser("list", help=list_filters.__doc__)
    list_parser.add_argument(
        "strings", nargs="*", help="List of strings to select filters"
    )
    list_parser.set_defaults(parser="list")

    add_parser = subparsers.add_parser("add", help=add_filters.__doc__)
    add_parser.add_argument("names", nargs="+", help="List of file names")
    add_parser.set_defaults(parser="add")

    plot_parser = subparsers.add_parser("plot", help=plot_filters.__doc__)
    plot_parser.add_argument("names", nargs="*", help="List of filter names")
    plot_parser.set_defaults(parser="plot")

    if len(sys.argv) == 1:
        parser.print_usage()
    else:
        args = parser.parse_args()
        if args.parser == "list":
            list_filters(args.strings)
        elif args.parser == "add":
            add_filters(args.names)
        elif args.parser == "plot":
            plot_filters(args.names)
